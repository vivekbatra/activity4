// *********************    Student Name - Vivek Batra Student Id - C0741344
// *********************    Student Name - Pintu Jat Student Id - C0744398
// *********************    Student Name - Chandra Keerthi Student Id - C0741753




package com.example.droidcafewsettings;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;
    private OrderActivity activityorder;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void checkToastWhenClickedOnYorgut() throws Exception {
        // Get the Froyo Image
        ImageView froYoImg = (ImageView) activity.findViewById(R.id.froyo);
        // Click on Froyo Image
        froYoImg.performClick();
        // Check the Toast you get is equal to desired one?
        assertThat(ShadowToast.getTextOfLatestToast().toString(), equalTo("You ordered a FroYo."));
    }



    @Test
    public void checkIfValuesAreRetained() {
        // Create Float Cart Button
        FloatingActionButton cartButton = (FloatingActionButton) activity.findViewById(R.id.fab);
        // Click on it
        cartButton.performClick();

        // Create a refrence to the OrderActivity
        activityorder = Robolectric.buildActivity(OrderActivity.class)
                .create()
                .resume()
                .get();

        // Get the name, address, phone, note edit text and set it to the values
        EditText name = (EditText) activityorder.findViewById(R.id.name_text);
        name.setText("Vivek");

        EditText address = (EditText) activityorder.findViewById(R.id.address_text);
        address.setText("319 Toynevale Rd, Pickering");

        EditText phone = (EditText) activityorder.findViewById(R.id.phone_text);
        phone.setText("4370000000");

        EditText note = (EditText) activityorder.findViewById(R.id.note_text);
        note.setText("My student ID is C0741344");

        // Get the radio button and click on it
        RadioButton pickup = (RadioButton) activityorder.findViewById(R.id.pickup);
        pickup.performClick();

        // Get the save button and click on it
        Button saveButton = (Button) activityorder.findViewById(R.id.saveButton);
        saveButton.performClick();

        // Press the back Button
        activityorder.onBackPressed();
        //Press again the cartButton
        cartButton.performClick();

        // Now assert the value to the expected ones
        assertEquals("Vivek", name.getText().toString());
        assertEquals("319 Toynevale Rd, Pickering", address.getText().toString());
        assertEquals("4370000000", phone.getText().toString());
        assertEquals("My student ID is C0741344", note.getText().toString());
        assertTrue(pickup.isChecked());

    }

}
